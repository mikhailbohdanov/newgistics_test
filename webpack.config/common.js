
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

const helpers = require('./helpers');


const NoEmitOnErrorsPlugin = webpack.NoEmitOnErrorsPlugin;
const ProvidePlugin = webpack.ProvidePlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = function () {
  return {
    context: helpers.src(),
    entry: {
      main: './main',
    },
    resolve: {
      extensions: ['.js'],
      modules: [helpers.src(), helpers.root('node_modules')],
    },
    resolveLoader: {
      extensions: ['.js'],
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          include: [helpers.src(), helpers.root('node_modules/react-input-colorpicker')],
          loader: 'babel-loader',
        },
        {
          test: /\.css$/,
          include: [helpers.src(), helpers.root('node_modules')],
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: false,
                  importLoaders: 1,
                }
              },
            ],
          }),
        },
        {
          test: /\.styl$/,
          include: [helpers.src()],
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[path]__[local]___[hash:base64:5]',
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: [
                    autoprefixer({
                      browsers: ['> 5%'],
                    }),
                  ],
                },
              },
              'stylus-loader',
            ]
          }),
        },
        {
          test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader'
        },
        {
          test: /\.(woff|woff2)$/,
          loader: 'url-loader?prefix=font/&limit=5000'
        },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
        },
        {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
        },
        {
          test: /\.gif/,
          loader: 'url-loader?limit=10000&mimetype=image/gif'
        },
        {
          test: /\.jpg/,
          loader: 'url-loader?limit=10000&mimetype=image/jpg'
        },
        {
          test: /\.png/,
          loader: 'url-loader?limit=10000&mimetype=image/png'
        }
      ],
    },

    plugins: [
      new NoEmitOnErrorsPlugin(),

      new ExtractTextPlugin({
        filename: 'style.css',
        allChunks: true,
        disable: true,
      }),

      new HtmlWebpackPlugin({
        template: helpers.src('index.html'),
        files: {
          css: ['style.css'],
          js: [ 'main.js'],
        }
      }),

      new ProvidePlugin({
        '_': ['lodash'],
      }),

    ],
  };
};
