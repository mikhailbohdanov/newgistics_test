/*
 * Main requires
 */
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const helpers = require('./helpers');
const commonConfig = require('./common');

const HotModuleReplacementPlugin = webpack.HotModuleReplacementPlugin;
const NamedModulesPlugin = webpack.NamedModulesPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');


// eslint-disable-next-line no-unused-vars
const ENV = process.env.ENV = process.env.NODE_ENV = 'develop';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;
// eslint-disable-next-line no-unused-vars
const HMR = helpers.hasProcessFlag('hot');


module.exports = function (options) {
  return webpackMerge(commonConfig(options), {
    devtool: 'cheap-inline-module-source-map',
    entry: {
      main: [
        'react-hot-loader/patch',
        `webpack-dev-server/client?http://${HOST}:${PORT}`,
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        './main'
      ],
    },
    output: {
      path: helpers.root('dist'),
      filename: '[name].js',
      sourceMapFilename: '[file].map',
      chunkFilename: '[id].chunk.js',
      publicPath: '/',
    },
    plugins: [
      new HotModuleReplacementPlugin(),
      new NamedModulesPlugin(),
      new CopyWebpackPlugin([
        {
          from: helpers.src('views', 'static'),
          to: 'static',
        }
      ]),
    ],
    devServer: {
      port: PORT,
      host: HOST,
      contentBase: './src',
      publicPath: '/',
      historyApiFallback: true,
      hot: true,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
      },
    },

  });
};
