import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import ConnectAll from 'core/utils/connect-all';

import SHIPMENT_STATUS from 'core/constants/shipment-status';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class TrackingWay extends Component {
  static propTypes = {
    status: PropTypes.string,
  };

  render() {
    const { status } = this.props;

    return (
      <div styleName="wrapper">
        <div styleName={classNames('preparing', { active: status === SHIPMENT_STATUS.PREPARING_ORDER })}>
          <img src="" />
          <span>Preparing Order</span>
        </div>
        <div styleName={classNames('shipped', { active: status === SHIPMENT_STATUS.SHIPPED })}>
          <img src="" />
          <span>Shipped</span>
        </div>
        <div styleName={classNames('out-for-delivery', { active: status === SHIPMENT_STATUS.OUT_FOR_DELIVERY })}>
          <img src="" />
          <span>Out For Delivery</span>
        </div>
        <div styleName={classNames('delivered', { active: status === SHIPMENT_STATUS.DELIVERED })}>
          <img src="" />
          <span>Delivered</span>
        </div>
      </div>
    );
  }
}
