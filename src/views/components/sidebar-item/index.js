import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import ConnectAll from 'core/utils/connect-all';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class SidebarItem extends Component {
  static propTypes = {
    className: PropTypes.string,
    link: PropTypes.string,
    title: PropTypes.string,
    image: PropTypes.string,
  };

  render() {
    const { className, link, title, image } = this.props;

    return (
      <NavLink className={className}
               styleName="wrapper"
               activeClassName={styles.active}
               to={link}>
        <div styleName="title">
          {title}
        </div>

        <div styleName="image">
          <img src={image} />
        </div>
      </NavLink>
    );
  }
}
