import React, {Component} from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import classNames from 'classnames';

import ConnectAll from 'core/utils/connect-all';

import MODULES from 'views/modules';

import Icon from 'views/components/icon';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class ModuleWrapper extends Component {
  static propTypes = {
    className: PropTypes.string,
    editMode: PropTypes.bool,
    module: PropTypes.object,
    moduleId: PropTypes.string,
    moduleType: PropTypes.string,
  };

  static defaultProps = {
    editMode: false,
  };

  constructor(props) {
    super(props);

    const { module, moduleType } = props;
    this.Component = _.get(MODULES, [moduleType, module.component]);

    this.state = {
      settingsOpen: false,
    };
  }

  @autobind
  toggleSettings() {
    const { settingsOpen } = this.state;

    this.setState({
      settingsOpen: !settingsOpen,
    });
  }

  get header() {
    if (!this.props.editMode) {
      return null;
    }

    const title = this.state.settingsOpen ? (
      <span styleName="header-title">
        Settings
      </span>
    ) : null;

    return (
      <div styleName="header">
        {title}

        <button styleName={classNames('header-button', { active: this.state.settingsOpen })}
                onClick={this.toggleSettings}>
          <Icon name="cog" />
        </button>
        <button styleName="header-button">
          <Icon name="refresh" />
        </button>
        <button styleName="header-button">
          <Icon name="times" />
        </button>
      </div>
    );
  }

  get settings() {
    if (!this.props.editMode || !this.state.settingsOpen) {
      return null;
    }

    const { moduleId, module } = this.props;
    const SettingsComponent = this.Component.SettingsComponent || this.Component.WrappedComponent.SettingsComponent;

    if (!SettingsComponent) {
      return null;
    }

    return (
      <div styleName="settings">
        <SettingsComponent moduleId={moduleId}
                           options={module.options} />
      </div>
    );
  }

  get component() {
    const { module } = this.props;

    return (
      <this.Component options={module.options} />
    );
  }

  render() {
    const { className } = this.props;

    return (
      <div className={className}>
        <div styleName={classNames('wrapper', { opened: this.state.settingsOpen })}>
          {this.header}
          {this.settings}

          <div styleName="component">
            {this.component}
          </div>
        </div>
      </div>
    );
  }
}
