import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';

import FontAwesome from 'react-fontawesome';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class Icon extends Component {
  static propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
  };

  render() {
    const { className, name } = this.props;

    return (
      <FontAwesome name={name}
                   className={className} />
    );
  }
}
