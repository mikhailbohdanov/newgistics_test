import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PIE_GRAPH_CONFIG from 'core/constants/highcharts-config/pie';

import nextColor from 'views/utils/color';

import { GraphCore } from './core';


export class GraphPie extends Component {
  static propTypes = {
    data: PropTypes.array,
    baseColor: PropTypes.string,
    offsetColor: PropTypes.string,
  };

  get data() {
    const {
      baseColor,
      offsetColor,
    } = this.props;
    let data = this.props.data;

    if (baseColor) {
      data = _.map(data, (item, index) => ({
        ...item,
        color: nextColor(baseColor, offsetColor, index),
      }));
    }

    return [{
      data,
    }];
  }

  render() {
    const props = _.omit(this.props, ['data', 'baseColor', 'offsetColor']);

    return (
      <GraphCore {...props}
                 data={this.data}
                 config={PIE_GRAPH_CONFIG} />
    );
  }
}
