import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactHighcharts from 'react-highcharts';


export class GraphCore extends Component {
  static propTypes = {
    title: PropTypes.string,
    data: PropTypes.array,
    config: PropTypes.object,
  };

  get title() {
    return {
      text: this.props.title,
    };
  }

  get config() {
    const { config, data } = this.props;

    return {
      ...config,
      credits: {
        enabled: false,
      },
      title: this.title,
      series: data,
    };
  }

  render() {
    const props = _.omit(this.props, ['title', 'data', 'config']);

    return (
      <ReactHighcharts {...props}
                       config={this.config} />
    );
  }
}
