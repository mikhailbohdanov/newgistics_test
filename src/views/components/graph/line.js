import React, { Component } from 'react';
import PropTypes from 'prop-types';

import LINE_GRAPH_CONFIG from 'core/constants/highcharts-config/line';

import nextColor from 'views/utils/color';

import { GraphCore } from './core';


export class GraphLine extends Component {
  static propTypes = {
    data: PropTypes.array,
    baseColor: PropTypes.string,
    offsetColor: PropTypes.string,
  };

  get data() {
    const {
      baseColor,
      offsetColor,
    } = this.props;
    let data = this.props.data;

    data = _.map(data, item => ({
      data: item,
    }));

    if (baseColor) {
      data = _.map(data, (item, index) => ({
        ...item,
        color: nextColor(baseColor, offsetColor, index),
      }));
    }

    return data;
  }

  render() {
    const props = _.omit(this.props, ['data', 'baseColor', 'offsetColor']);

    return (
      <GraphCore {...props}
                 data={this.data}
                 config={LINE_GRAPH_CONFIG} />
    );
  }
}
