import client from './client';
import analytics from './analytics';


export default {
  client,
  analytics,
};
