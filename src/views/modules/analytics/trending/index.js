import React, {Component} from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { updateOptions } from 'core/redux/actions/modules/analytics';

import { SketchPicker } from 'react-color';

import { GraphLine } from 'views/components/graph';

import styles from './index.styl';


const deviceData = [
  [ 30, 20, 15, 64, 24, 74 ],
  [ 15, 67, 25, 25, 78, 35 ],
];


@ConnectAll(null, {
  updateOptions,
}, styles)
class Settings extends Component {
  static propTypes = {
    moduleId: PropTypes.string,
    options: PropTypes.object,
    updateOptions: PropTypes.func,
  };

  static defaultProps = {
    updateOptions: _.noop,
  };

  constructor(props) {
    super(props);

    const { options } = props;

    this.state = {
      options,
    };
  }

  updateOptions() {
    const { moduleId, updateOptions } = this.props;

    updateOptions(moduleId, this.state.options);
  }

  @autobind
  handleOptionChange(key, value) {
    const { options } = this.state;

    this.setState({
      options: {
        ...options,
        [key]: value,
      },
    });

    this.updateOptions();
  }

  @autobind
  handleColorChange(colorName) {
    return color => this.handleOptionChange(colorName, color.hex);
  }

  get baseColorPicker() {
    const { baseColor } = this.state.options;

    return (
      <SketchPicker color={baseColor}
                    onChangeComplete={this.handleColorChange('baseColor')} />
    );
  }

  get offsetColorPicker() {
    const { offsetColor } = this.state.options;

    return (
      <SketchPicker color={offsetColor}
                    onChangeComplete={this.handleColorChange('offsetColor')} />
    );
  }

  render() {
    return (
      <div styleName="settings">
        {this.baseColorPicker}
        {this.offsetColorPicker}
      </div>
    );
  }
}


@ConnectAll(null, null, styles)
export default class Trending extends Component {
  static SettingsComponent = Settings;

  static propTypes = {
    options: PropTypes.object,
  };

  get deviceType() {
    const { baseColor, offsetColor } = this.props.options;

    return (
      <div>
        <GraphLine data={deviceData}
                   baseColor={baseColor}
                   offsetColor={offsetColor} />
      </div>
    );
  }

  render() {
    return (
      <div styleName="wrapper">
        <div styleName="header">
          What are the platforms?
        </div>
        <div styleName="sub-header">
          Showing the number of user over the past 30 days
        </div>

        {this.deviceType}
      </div>
    );
  }
}
