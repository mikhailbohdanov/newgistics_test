import React, {Component} from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { SketchPicker } from 'react-color';

import { updateOptions } from 'core/redux/actions/modules/analytics';

import { GraphPie } from 'views/components/graph';

import styles from './index.styl';


const deviceData = [
  { y: 30, name: 'Mobile' },
  { y: 20, name: 'Tablets' },
  { y: 50, name: 'Desktop' },
];
const brandData = [
  { y: 30, name: 'Samsung' },
  { y: 20, name: 'LG' },
  { y: 50, name: 'Apple' },
  { y: 40, name: 'Microsoft' },
];


@ConnectAll(null, {
  updateOptions,
}, styles)
class Settings extends Component {
  static propTypes = {
    moduleId: PropTypes.string,
    options: PropTypes.object,
    updateOptions: PropTypes.func,
  };

  static defaultProps = {
    updateOptions: _.noop,
  };

  constructor(props) {
    super(props);

    const { options } = props;

    this.state = {
      options,
    };
  }

  updateOptions() {
    const { moduleId, updateOptions } = this.props;

    updateOptions(moduleId, this.state.options);
  }

  @autobind
  handleOptionChange(key, value) {
    const { options } = this.state;

    this.setState({
      options: {
        ...options,
        [key]: value,
      },
    });

    this.updateOptions();
  }

  @autobind
  handleColorChange(colorName) {
    return color => this.handleOptionChange(colorName, color.hex);
  }

  get baseColorPicker() {
    const { baseColor } = this.state.options;

    return (
      <SketchPicker color={baseColor}
                    onChangeComplete={this.handleColorChange('baseColor')} />
    );
  }

  get offsetColorPicker() {
    const { offsetColor } = this.state.options;

    return (
      <SketchPicker color={offsetColor}
                    onChangeComplete={this.handleColorChange('offsetColor')} />
    );
  }

  render() {
    return (
      <div styleName="settings">
        {this.baseColorPicker}
        {this.offsetColorPicker}
      </div>
    );
  }
}


@ConnectAll(null, null, styles)
export default class Platforms extends Component {
  static SettingsComponent = Settings;

  static propTypes = {
    options: PropTypes.object,
  };

  get deviceType() {
    const { baseColor, offsetColor } = this.props.options;

    return (
      <div>
        <GraphPie data={deviceData}
                  baseColor={baseColor}
                  offsetColor={offsetColor} />
      </div>
    );
  }

  get brandType() {
    const { baseColor, offsetColor } = this.props.options;

    return (
      <div>
        <GraphPie data={brandData}
                  baseColor={baseColor}
                  offsetColor={offsetColor} />
      </div>
    );
  }

  render() {
    return (
      <div styleName="wrapper">
        <div styleName="header">
          What are the platforms?
        </div>
        <div styleName="sub-header">
          Showing the number of user over the past 30 days
        </div>

        <div styleName="grahps">
          {this.deviceType}
          {this.brandType}
        </div>
      </div>
    );
  }
}
