import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';
import { getData } from 'core/redux/actions/modules/analytics';
import { selectModules } from 'core/redux/selectors/modules';

import ModuleWrapper from 'views/components/module-wrapper';


const MODULES_TYPE = 'analytics';


@ConnectAll(state => ({
  modules: selectModules(state, MODULES_TYPE),
}), {
  getData,
})
export default class Analytics extends Component {
  static propTypes = {
    modules: PropTypes.object,
    getData: PropTypes.func,
    editMode: PropTypes.bool,
  };

  static defaultProps = {
    getData: _.noop,
    editMode: true,
  };

  componentWillMount() {
    this.props.getData();
  }

  render() {
    const { editMode, modules } = this.props;

    return (
      <div className="row">
        {_.map(modules, (module, moduleId) => (
          <ModuleWrapper key={moduleId}
                         className="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                         editMode={editMode}
                         moduleType={MODULES_TYPE}
                         moduleId={moduleId}
                         module={module} />
        ))}
      </div>
    );
  }
}
