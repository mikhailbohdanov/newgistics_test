import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import classNames from 'classnames';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import SidebarItem from 'views/components/sidebar-item';

import Analytics from './components/analytics';
import Configuration from './components/configuration';
import Icon from 'views/components/icon';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class DeliveriesPage extends Component {

  state = {
    editModeEnabled: false,
  };

  @autobind
  toggleEditMode() {
    this.setState({
      editModeEnabled: !this.state.editModeEnabled,
    });
  }

  get sidebar() {
    return (
      <div className="col-lg-2 col-md-2 col-sm-12 col-xs-12"
           styleName="sidebar">
        <SidebarItem styleName="sidebar-item"
                     link="/deliveries/analytics"
                     title="Analytics"
                     image="/static/images/analytics.png" />
        <SidebarItem styleName="sidebar-item"
                     link="/deliveries/configuration"
                     title="Configuration"
                     image="/static/images/configuration.png" />
      </div>
    );
  }

  get header() {
    const { editModeEnabled } = this.state;
    return (
      <div styleName="header">
        <button styleName={classNames('header-button', { active: editModeEnabled })}
                onClick={this.toggleEditMode}>
          <Icon styleName="header-button-icon"
                name="edit" />
          {editModeEnabled ? 'Disable' : 'Enable'} edit mode
        </button>
      </div>
    );
  }

  render() {
    const { editModeEnabled } = this.state;

    return (
      <div className="row"
           styleName="wrapper">
        {this.sidebar}

        <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12"
             styleName="content">
          {this.header}

          <Route path="/deliveries/analytics" render={() => (
            <Analytics editMode={editModeEnabled} />
          )} />
          <Route path="/deliveries/configuration" component={Configuration} />
        </div>
      </div>
    );
  }
}
