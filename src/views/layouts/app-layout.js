import React, { Component } from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';

import routes from 'views/routes';

import Icon from 'views/components/icon';

import styles from './app-layout.styl';


const mainMenuLinks = [
  { url: '/', label: 'Home', exact: true },
  { url: '/deliveries', label: 'Deliveries' },
  { url: '/returns', label: 'Returns' },
  { url: '/fulfillment', label: 'Fulfillment' },
  { url: '/e-commerce', label: 'E-Commerce' },
];


@withRouter
@ConnectAll(() => ({
  user: {
    userName: 'John Smit',
  }
}), null, styles)
export default class AppLayout extends Component {
  static propTypes = {
    user: PropTypes.object,
  };

  static defaultProps = {
    signOut: _.noop,
  };

  get header() {
    const mainMenuItems = _.map(mainMenuLinks, ({ url, label, exact }, index) => (
      <NavLink key={index}
               to={url}
               exact={exact}
               styleName="nav-link"
               activeClassName={styles.active}>
        {label}
      </NavLink>
    ));

    return (
      <div styleName="header-wrapper">
        <div styleName="top-header">
          <div>
            <Icon styleName="top-header-icon"
                  name="bars" />
            NEWGISTICS SMART CENTER
          </div>
          <div styleName="search-bar">
            <div styleName="search-icon">
              <Icon name="search" />
            </div>
            <input styleName="search-input"
                   placeholder="Search" />
          </div>
          <div styleName="logo">
            Victoria's Secret
          </div>
        </div>
        <div styleName="bottom-header">
          <div styleName="main-menu">
            {mainMenuItems}
          </div>
          {this.userMenu}
        </div>
      </div>
    );
  }

  get subHeader() {
    return (
      <div styleName="sub-header">

      </div>
    );
  }

  get userMenu() {
    const { user } = this.props;

    if (!user) {
      return null;
    }

    return (
      <div styleName="user-menu">
        <NavLink to="/options"
                 styleName="user-menu-link">
          <Icon styleName="user-menu-icon"
                name="sliders" />
          Options
        </NavLink>
        <NavLink to="/options"
                 styleName="user-menu-link">
          <Icon styleName="user-menu-icon"
                name="user" />
          {user.userName}
        </NavLink>
        <NavLink to="/logout"
                 styleName="user-menu-link">
          <Icon name="sign-out" />
        </NavLink>
      </div>
    );
  }

  get footer() {
    return null;
  }

  render() {
    return (
      <div styleName="layout-wrapper">
        {this.header}
        {this.subHeader}
        {routes}
        {this.footer}
      </div>
    );
  }
}
