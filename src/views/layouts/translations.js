import { defineMessages } from 'react-intl';


export default defineMessages({
  myProfile: {
    id: 'layout.app.main-menu.my-profile',
    defaultMessage: 'My profile',
  },
  messages: {
    id: 'layout.app.main-menu.messages',
    defaultMessage: 'Messages',
  },
  settings: {
    id: 'layout.app.main-menu.settings',
    defaultMessage: 'Settings',
  },
  signOut: {
    id: 'layout.app.main-menu.sign-out',
    defaultMessage: 'Sign out',
  },

});
