import React from 'react';
import { Route } from 'react-router-dom';

import MainPage from 'views/pages/main';
import DeliveriesPage from 'views/pages/deliveries';


export default (
  <div>
    <Route exact path="/" component={MainPage} />
    <Route path="/deliveries" component={DeliveriesPage} />
  </div>
);
