
const HEX_COLOR = /^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i;

function parseHexColor(color) {
  if (HEX_COLOR.test(color)) {
    const [, ...colors] = HEX_COLOR.exec(color);
    return _.map(colors, color => parseInt(color, 16));
  }

  return [0, 0, 0];
}

function toHexColor(colors) {
  colors = _.map(colors, color => (`0${color.toString(16)}`).slice(-2));
  return `#${colors.join('')}`;
}


export default function (currentColor, offset, multiple) {
  currentColor = parseHexColor(currentColor);
  offset = parseHexColor(offset);

  let outColorNumber = _.map(currentColor, (color, index) => (color + (offset[index] * multiple)));
  return toHexColor(outColorNumber);
}
