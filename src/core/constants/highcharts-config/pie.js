export default {
  chart: {
    backgroundColor: 'transparent',
    plotBackgroundColor: 'transparent',
    type: 'pie',
  },
  tooltip: {
    enabled: false,
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: false,
      },
      showInLegend: true,
    },
  },
};
