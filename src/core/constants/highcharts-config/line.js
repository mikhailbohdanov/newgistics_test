export default {
  chart: {
    backgroundColor: 'transparent',
    plotBackgroundColor: 'transparent',
    type: 'line'
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
};
