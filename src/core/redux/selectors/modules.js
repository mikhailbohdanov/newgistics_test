
export function selectModules(state, type) {
  return _.get(state, ['modules', type]);
}
