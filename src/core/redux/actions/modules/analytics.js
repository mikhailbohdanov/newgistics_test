import { firebaseDb } from 'core/firebase';

import createAction from 'core/utils/create-async-action';


export const MODULES_ANALYTICS_GET = createAction('SERVER.MODULES.GET');
const {
  START: START_GET,
  SUCCESS: SUCCESS_GET,
  FAIL: FAIL_GET,
} = MODULES_ANALYTICS_GET;

export const MODULES_ANALYTICS_OPTIONS_UPDATE = createAction('SERVER.MODULES.OPTIONS.UPDATE');
const {
  START: OPTIONS_UPDATE_START,
  SUCCESS: OPTIONS_UPDATE_SUCCESS,
  FAIL: OPTIONS_UPDATE_FAIL,
} = MODULES_ANALYTICS_OPTIONS_UPDATE;


export function getData() {
  return function (dispatch) {
    dispatch(START_GET());

    return firebaseDb
      .ref('modules/analytics')
      .once('value', data => {
        const value = data.val();

        if (value) {
          dispatch(SUCCESS_GET(null, value));
          return Promise.resolve(value);
        } else {
          dispatch(FAIL_GET());
          return Promise.reject(null);
        }
      });
  };
}

export function updateOptions(moduleId, options) {
  const params = {
    moduleId,
    options,
  };

  return function (dispatch) {
    dispatch(OPTIONS_UPDATE_START(params));

    firebaseDb
      .ref(`modules/analytics/${moduleId}/options`)
      .update(options)
      .then(() => {
        dispatch(OPTIONS_UPDATE_SUCCESS(params));
      });
  };
}
