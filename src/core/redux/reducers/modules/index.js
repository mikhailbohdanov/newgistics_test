import { combineReducers } from 'redux';

import analytics from './analytics';


export default combineReducers({
  analytics,
});
