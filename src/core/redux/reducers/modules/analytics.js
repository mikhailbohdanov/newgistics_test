import { handleActions } from 'redux-actions';

import {
  MODULES_ANALYTICS_GET,
  MODULES_ANALYTICS_OPTIONS_UPDATE,
} from 'core/redux/actions/modules/analytics';


const DEFAULT_STATE = {};


export default handleActions({
  [MODULES_ANALYTICS_GET.SUCCESS]: (state, { payload: { response }}) => response,
  [MODULES_ANALYTICS_OPTIONS_UPDATE.SUCCESS]: (state, { payload: { params } }) => {
    const { moduleId, options } = params;

    const module = state[moduleId];

    return {
      ...state,
      [moduleId]: {
        ...module,
        options,
      }
    };
  }
}, DEFAULT_STATE);
