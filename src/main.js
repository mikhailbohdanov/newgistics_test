import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { IntlProvider } from 'react-intl';

import configureStore from 'core/redux/store';

import Root from 'views/root';

import 'views/styles/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';


const store = configureStore();

const rootElement = document.getElementById('root');


function render(RootElement) {
  ReactDOM.render(
    <AppContainer>
      <IntlProvider locale="en">
        <RootElement store={store} />
      </IntlProvider>
    </AppContainer>,
    rootElement
  );
}

if (module.hot) {
  module.hot.accept('views/root', () => {
    render(require('views/root').default);
  });
}

render(Root);
